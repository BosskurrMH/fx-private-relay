# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Mobile menu icons 

menu-upgrade-button = Upgrade 
menu-toggle-open = Open menu
menu-toggle-close = Close menu
nav-dashboard = Dashboard
nav-settings = Settings
nav-support = Help and Support 
nav-sign-out = Sign Out
nav-contact = Contact Us

landing-use-cases-shopping = Shopping
landing-use-cases-shopping-hero-heading = Shopping with email masks
landing-use-cases-shopping-hero-content1 = Want to buy something online and don’t know or fully trust the shop?
landing-use-cases-shopping-hero-content2 = Use an email mask whenever you shop online. Get the confirmation sent to your real email and then easily turn the mask off anytime later.

landing-use-cases-on-the-go = On the Go
landing-use-cases-on-the-go-heading = On the go with { -brand-name-relay }
landing-use-cases-on-the-go-lead = Instantly make a custom email mask anywhere and everywhere you go!
landing-use-cases-on-the-go-connect-heading = Connect on the go
landing-use-cases-on-the-go-connect-body = Use your email mask when you want to privately sign into your favorite coffee shop or public wifi
landing-use-cases-on-the-go-receipt-heading = Get email receipts
landing-use-cases-on-the-go-receipt-body = Share a custom email mask for in-store shopping receipts without sharing your real email
landing-use-cases-on-the-go-phone-heading = Use on your phone
landing-use-cases-on-the-go-phone-body = No matter where you are create a custom email mask in seconds for anything you want to do

landing-use-cases-signups = Signups
landing-use-cases-signups-hero-heading = Worry-free signups
landing-use-cases-signups-hero-content1 = Want to start a new subscription, RSVP to an event, or get a bargain promo code without having spam flooding your inbox?
landing-use-cases-signups-hero-content2 = Before you complete that next signup, use an email mask instead of your real one to protect your info and keep control over your inbox.

banner-download-install-chrome-extension-copy-2 = The { -brand-name-firefox-relay } extension for { -brand-name-chrome } makes creating and using masks even easier.
multi-part-onboarding-premium-chrome-extension-get-description-2 = The { -brand-name-firefox-relay } extension for { -brand-name-chrome } makes creating and using email masks even easier.

setting-api-key-copied = Copied!

profile-promo-email-blocking-option-promotionals-premiumonly-marker = ({ -brand-name-premium } only)
profile-promo-email-blocking-description-promotionals-locked-label = Available to { -brand-name-relay-premium } subscribers
profile-promo-email-blocking-description-promotionals-locked-cta = Upgrade now
profile-promo-email-blocking-description-promotionals-locked-waitlist-cta = Join the { -brand-name-relay-premium } wait list
profile-promo-email-blocking-description-promotionals-locked-close = Close

popover-custom-alias-explainer-promotional-block-tooltip-trigger = More info

tips-toast-button-expand-label = Learn more

waitlist-heading = Join the { -brand-name-relay-premium } Waitlist
waitlist-lead = Get notified when { -brand-name-firefox-relay-premium } is available for your region.
waitlist-control-required = Required
waitlist-control-email-label = What is your email address?
waitlist-control-email-placeholder = yourname@example.com
waitlist-control-country-label = What country do you live in?
waitlist-control-locale-label = Select your preferred language.
waitlist-submit-label = Join the Waitlist
waitlist-privacy-policy-agree = By clicking “{ waitlist-submit-label }”, you agree to our <a>Privacy Policy</a>.
waitlist-privacy-policy-use = Your information will only be used to notify you about { -brand-name-firefox-relay-premium } availability.
waitlist-subscribe-success = You’re on the list! Once { -brand-name-firefox-relay-premium } becomes available for your region, we’ll email you.
waitlist-subscribe-error-connection = There was an error adding you to the waitlist. Please check your connection, then try again.
waitlist-subscribe-error-unknown = There was an error adding you to the waitlist. Please try again.

## Notifications component

toast-button-close-label = Close notification
success-signed-out-message = You have signed out.
success-signed-in-message = Successfully signed in as { $username }.

whatsnew-feature-premium-expansion-sweden-heading = { -brand-name-relay-premium } available in Sweden
whatsnew-feature-premium-expansion-finland-heading = { -brand-name-relay-premium } available in Finland
# A preview of the full content of `whatsnew-feature-premium-sweden-description`.
# When translating, please make sure the resulting string is of roughly similar
# length as the English version.
whatsnew-feature-premium-expansion-snippet = Upgrade now and get even more protection…
whatsnew-feature-premium-expansion-description = Upgrade now and get even more protection — create unlimited email masks, get a custom email subdomain, and more!

fx-desktop-2 = { -brand-name-firefox } for Desktop
fx-mobile-2 = { -brand-name-firefox } for Mobile
fx-containers = { -brand-name-firefox } Containers


profile-label-replies = Replies
profile-replies-tooltip = You can reply to emails received through this mask, and Firefox Relay will continue to protect your true email address.
